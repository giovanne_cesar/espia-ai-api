<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('school_id');
            $table->boolean('is_teacher');
            $table->string('username', 30)->unique();
            $table->string('password');
            $table->string('matriculation_number');
            $table->string('name', 30);
            $table->string('photo', 100);
            $table->string('email')->unique();
            $table->string('phone');
            $table->timestamps();

            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('school_id')->references('id')->on('schools');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
