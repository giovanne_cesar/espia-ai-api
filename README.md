# Espia Aí API

## Instalação

Iniciando as dependências do laradock
```
git submodule init
git submodule update
```

Copiando arquivos de configuração
```
cp .env.example .env
cp .laradock.env laradock/.env
```

Baixando imagens e iniciando o container
```
cd laradock
docker-compose up -d mysql nginx
```

Configurando o ambiente
```
docker-compose exec workspace bash
composer install
artisan key:generate
artisan migrate:fresh --seed
```
