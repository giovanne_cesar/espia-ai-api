<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolYear extends Model
{
    use HasFactory;
    protected $fillable = ['school_id', 'description'];

    public function rules() {
        return [
            'school_id' => 'exists:schools,id',
        ];
    }

    public function studentClasses() {
        return $this->hasMany('App\Models\StudentClass');
    }

    public function school() {
        return $this->belongsTo('App\Models\School');
    }
}
