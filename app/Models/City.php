<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;
    protected $fillable = ['name'];

    public function rules() {
        return [
            'name' => 'required|unique:cities,name,'.$this->id.'|min:3',
        ];
    }

    public function schools() {
        return $this->hasMany('App\Models\School');
    }
}
