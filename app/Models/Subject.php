<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'workload'];

    public function rules() {
        return [
            'name' => 'required|unique:subjects,name,'.$this->id.'|min:3',
        ];
    }

    public function subjectClasses() {
        return $this->hasMany('App\Models\SubjectClass');
    }
}
