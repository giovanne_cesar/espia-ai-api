<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $fillable = ['school_id', 'student_class_id', 'username', 'password', 'matriculation_number', 'name',
        'photo', 'email', 'phone'];

    public function rules() {
        return [
            'username' => 'required|unique:students,username,'.$this->id.'|min:3',
            'school_id' => 'exists:schools,id',
            'student_class_id' => 'exists:student_classes,id',
            'photo' => 'file|mimes:png,jpeg,jpg'
        ];

    }

    public function school(){
        return $this->belongsTo('App\Models\School');
    }

    public function studentClass(){
        return $this->belongsTo('App\Models\StudentClass');
    }
}
