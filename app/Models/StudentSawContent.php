<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentSawContent extends Model
{
    use HasFactory;
    protected $fillable = ['student_id', 'content_id'];

    public function rules() {
        return [
            'student_id' => 'exists:students,id',
            'content_id' => 'exists:contents,id',
        ];
    }

    public function content() {
        return $this->belongsTo('App\Models\Content');
    }

    public function student() {
        return $this->belongsTo('App\Models\Student');
    }
}
