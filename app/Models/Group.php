<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'hierarchy'];

    public function rules() {
        return [
            'name' => 'required|unique:groups,name,'.$this->id.'|min:3',
            'hierarchy' => 'required|integer|digits_between:1,20',
        ];
    }

    public function roles() {
        return $this->hasMany('App\Models\Role');
    }
}
