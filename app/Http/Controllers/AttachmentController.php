<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AttachmentController extends Controller {

    public function Attachment($path)
    {
        return response()->file('storage/'.$path);
    }
}
