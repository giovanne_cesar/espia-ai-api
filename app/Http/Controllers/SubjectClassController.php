<?php

namespace App\Http\Controllers;

use App\Models\SubjectClass;
use Illuminate\Http\Request;
use App\Repositories\SubjectClassRepository;
use App\Models\Employee;

class SubjectClassController extends Controller
{
    public function __construct(SubjectClass $subject_class, Employee $employee) {
        $this->subject_class = $subject_class;
        $this->employee = $employee;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $subject_classRepository = new SubjectClassRepository($this->subject_class);

        if($request->has('filtro')) {
            $subject_classRepository->filtro($request->filtro);
        }

        if($request->has('atributos')) {
            $subject_classRepository->selectAtributos($request->atributos);
        }
        return response()->json($subject_classRepository->getResultado(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->subject_class->rules());
        $employee = $this->employee->find($request->employee_id);

        if($employee->is_teacher === 0){
            abort(404);
        }

        $subject_class = $this->subject_class->create([
            'employee_id' => $request->employee_id,
            'subject_id' => $request->subject_id,
            'student_class_id' => $request->student_class_id,
        ]);
        return response()->json($subject_class, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubjectClass  $subject_class
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subject_class = $this->subject_class->with(['employee', 'subject', 'studentClass', 'contents'])->find($id);
        if($subject_class === null) {
            return response()->json(['erro' => 'Recurso pesquisado não existe'], 404) ;
        }
        return response()->json($subject_class, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubjectClass  $subject_class
     * @return \Illuminate\Http\Response
     */
    public function edit(SubjectClass $subject_class)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubjectClass  $subject_class
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subject_class = $this->subject_class->find($id);

        if($subject_class === null) {
            return response()->json(['erro' => 'Impossível realizar a atualização. O recurso solicitado não existe'],
                404);
        }

        if($request->method() === 'PATCH') {

            $regrasDinamicas = array();

            //percorrendo todas as regras definidas no Model
            foreach($subject_class->rules() as $input => $regra) {

                //coletar apenas as regras aplicáveis aos parâmetros parciais da requisição PATCH
                if(array_key_exists($input, $request->all())) {
                    $regrasDinamicas[$input] = $regra;
                }
            }

            $request->validate($regrasDinamicas);

        } else {
            $request->validate($subject_class->rules());
        }

        $subject_class->fill($request->all());
        $subject_class->save();

        return response()->json($subject_class, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubjectClass  $subject_class
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subject_class = $this->subject_class->find($id);

        if($subject_class === null) {
            return response()->json(['erro' => 'Impossível realizar a exclusão. O recurso solicitado não existe'], 404);
        }

        $subject_class->delete();
        return response()->json(['msg' => 'O modelo foi removido com sucesso!'], 200);

    }
}
