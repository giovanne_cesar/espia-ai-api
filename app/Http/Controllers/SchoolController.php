<?php

namespace App\Http\Controllers;

use App\Models\School;
use Illuminate\Http\Request;
use App\Repositories\SchoolRepository;
use Illuminate\Support\Facades\Storage;

class SchoolController extends Controller
{
    public function __construct(School $school) {
        $this->school = $school;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $schoolRepository = new SchoolRepository($this->school);

        if($request->has('filtro')) {
            $schoolRepository->filtro($request->filtro);
        }

        if($request->has('atributos')) {
            $schoolRepository->selectAtributos($request->atributos);
        }

        return response()->json($schoolRepository->getResultado(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->school->rules());
        if($request->file('photo')) {
            $photo = $request->file('photo');
            $photo_urn = $photo->store('/', 'public');
        }else{
            $photo_urn = null;
        }

        $school = $this->school->create([
            'city_id' => $request->city_id,
            'name' => $request->name,
            'public_place' => $request->public_place,
            'public_place_number' => $request->public_place_number,
            'CEP' => $request->CEP,
            'phone' => $request->phone,
            'photo' => $photo_urn,
        ]);


        return response()->json($school, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $school = $this->school->with(['employees', 'schoolAreas', 'city', 'schoolYears', 'students'])->find($id);
        if($school === null) {
            return response()->json(['erro' => 'Recurso pesquisado não existe'], 404) ;
        }

        return response()->json($school, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */
    public function edit(School $school)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $school = $this->school->find($id);

        if($school === null) {
            return response()->json(['erro' => 'Impossível realizar a atualização. O recurso solicitado não existe'],
                404);
        }

        if($request->method() === 'PATCH') {

            $regrasDinamicas = array();

            //percorrendo todas as regras definidas no Model
            foreach($school->rules() as $input => $regra) {

                //coletar apenas as regras aplicáveis aos parâmetros parciais da requisição PATCH
                if(array_key_exists($input, $request->all())) {
                    $regrasDinamicas[$input] = $regra;
                }
            }

            $request->validate($regrasDinamicas);

        } else {
            $request->validate($school->rules());
        }
        if($request->file('photo')) {
            Storage::disk('public')->delete($school->photo);
            $photo = $request->file('photo');
            $photo_urn = $photo->store('/', 'public');
        }else{
            $photo_urn = null;
        }

        $school->fill($request->all());
        $school->photo = $photo_urn;
        $school->save();

        return response()->json($school, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $school = $this->school->find($id);

        if($school === null) {
            return response()->json(['erro' => 'Impossível realizar a exclusão. O recurso solicitado não existe'], 404);
        }
        if ($school->photo != null) {
            Storage::disk('public')->delete($school->photo);
        }

        $school->delete();
        return response()->json(['msg' => 'O modelo foi removido com sucesso!'], 200);

    }
}
