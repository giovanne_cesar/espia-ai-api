<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageController extends Controller {

    public function Picture($path)
    {
        return response()->file('storage/'.$path);
    }
}
