<?php

namespace App\Http\Controllers;

use App\Models\Content;
use Illuminate\Http\Request;
use App\Repositories\ContentRepository;
use Illuminate\Support\Facades\Storage;

class ContentController extends Controller
{
    public function __construct(Content $content) {
        $this->content = $content;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $contentRepository = new ContentRepository($this->content);

        if($request->has('filtro')) {
            $contentRepository->filtro($request->filtro);
        }

        if($request->has('atributos')) {
            $contentRepository->selectAtributos($request->atributos);
        }
        return response()->json($contentRepository->getResultado(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->content->rules());
        if($request->file('attachment')) {
            $attachment = $request->file('attachment');
            $attachment_urn = $attachment->store('attachments', 'public');
        }else{
            $attachment_urn = null;
        }

        $content = $this->content->create([
            'name' => $request->name,
            'description' => $request->description,
            'subject_class_id' => $request->subject_class_id,
            'attachment' => $attachment_urn,
            'date' => $request->date,
        ]);
        return response()->json($content, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $content = $this->content->with('subjectClass', 'studentsSawContent')->find($id);
        if($content === null) {
            return response()->json(['erro' => 'Recurso pesquisado não existe'], 404) ;
        }
        return response()->json($content, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function edit(Content $content)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $content = $this->content->find($id);

        if($content === null) {
            return response()->json(['erro' => 'Impossível realizar a atualização. O recurso solicitado não existe'],
                404);
        }

        if($request->method() === 'PATCH') {

            $regrasDinamicas = array();

            //percorrendo todas as regras definidas no Model
            foreach($content->rules() as $input => $regra) {

                //coletar apenas as regras aplicáveis aos parâmetros parciais da requisição PATCH
                if(array_key_exists($input, $request->all())) {
                    $regrasDinamicas[$input] = $regra;
                }
            }

            $request->validate($regrasDinamicas);

        } else {
            $request->validate($content->rules());
        }
        if($request->file('attachment')) {
            Storage::disk('public')->delete($content->attachment);
            $attachment = $request->file('attachment');
            $attachment_urn = $attachment->store('attachments', 'public');

        }else{
            $attachment_urn = null;
        }

        $content->fill($request->all());
        $content->attachment = $attachment_urn;
        $content->save();

        return response()->json($content, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $content = $this->content->find($id);

        if($content === null) {
            return response()->json(['erro' => 'Impossível realizar a exclusão. O recurso solicitado não existe'], 404);
        }
        if ($content->attachment != null) {
            Storage::disk('public')->delete($content->attachment);
        }

        $content->delete();
        return response()->json(['msg' => 'O modelo foi removido com sucesso!'], 200);

    }
}
