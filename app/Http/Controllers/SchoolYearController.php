<?php

namespace App\Http\Controllers;

use App\Models\SchoolYear;
use Illuminate\Http\Request;
use App\Repositories\SchoolYearRepository;

class SchoolYearController extends Controller
{
    public function __construct(SchoolYear $school_year) {
        $this->school_year = $school_year;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_yearRepository = new SchoolYearRepository($this->school_year);

        if($request->has('filtro')) {
            $school_yearRepository->filtro($request->filtro);
        }

        if($request->has('atributos')) {
            $school_yearRepository->selectAtributos($request->atributos);
        }

        return response()->json($school_yearRepository->getResultado(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->school_year->rules());

        $school_year = $this->school_year->create([
            'school_id' => $request->school_id,
            'description' => $request->description,
        ]);


        return response()->json($school_year, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SchoolYear  $school_year
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $school_year = $this->school_year->with(['studentClasses', 'school'])->find($id);
        if($school_year === null) {
            return response()->json(['erro' => 'Recurso pesquisado não existe'], 404) ;
        }

        return response()->json($school_year, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SchoolYear  $school_year
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolYear $school_year)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SchoolYear  $school_year
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $school_year = $this->school_year->find($id);

        if($school_year === null) {
            return response()->json(['erro' => 'Impossível realizar a atualização. O recurso solicitado não existe'],
                404);
        }

        if($request->method() === 'PATCH') {

            $regrasDinamicas = array();

            //percorrendo todas as regras definidas no Model
            foreach($school_year->rules() as $input => $regra) {

                //coletar apenas as regras aplicáveis aos parâmetros parciais da requisição PATCH
                if(array_key_exists($input, $request->all())) {
                    $regrasDinamicas[$input] = $regra;
                }
            }

            $request->validate($regrasDinamicas);

        } else {
            $request->validate($school_year->rules());
        }

        $school_year->fill($request->all());
        $school_year->save();

        return response()->json($school_year, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SchoolYear  $school_year
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $school_year = $this->school_year->find($id);

        if($school_year === null) {
            return response()->json(['erro' => 'Impossível realizar a exclusão. O recurso solicitado não existe'], 404);
        }

        $school_year->delete();
        return response()->json(['msg' => 'O modelo foi removido com sucesso!'], 200);

    }
}
