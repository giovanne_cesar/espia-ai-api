<?php

namespace App\Http\Controllers;

use App\Models\SchoolArea;
use Illuminate\Http\Request;
use App\Repositories\SchoolAreaRepository;
use Illuminate\Support\Facades\Storage;

class SchoolAreaController extends Controller
{
    public function __construct(SchoolArea $school_area) {
        $this->school_area = $school_area;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_areaRepository = new SchoolAreaRepository($this->school_area);

        if($request->has('filtro')) {
            $school_areaRepository->filtro($request->filtro);
        }

        if($request->has('atributos')) {
            $school_areaRepository->selectAtributos($request->atributos);
        }

        return response()->json($school_areaRepository->getResultado(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->school_area->rules());
        $photo = $request->file('photo');
        $photo_urn = $photo->store('/', 'public');

        $school_area = $this->school_area->create([
            'school_id' => $request->school_id,
            'name' => $request->name,
            'description' => $request->description,
            'photo' => $photo_urn,
        ]);


        return response()->json($school_area, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SchoolArea  $school_area
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $school_area = $this->school_area->with('school')->find($id);
        if($school_area === null) {
            return response()->json(['erro' => 'Recurso pesquisado não existe'], 404) ;
        }

        return response()->json($school_area, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SchoolArea  $school_area
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolArea $school_area)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SchoolArea  $school_area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $school_area = $this->school_area->find($id);

        if($school_area === null) {
            return response()->json(['erro' => 'Impossível realizar a atualização. O recurso solicitado não existe'],
                404);
        }

        if($request->method() === 'PATCH') {

            $regrasDinamicas = array();

            //percorrendo todas as regras definidas no Model
            foreach($school_area->rules() as $input => $regra) {

                //coletar apenas as regras aplicáveis aos parâmetros parciais da requisição PATCH
                if(array_key_exists($input, $request->all())) {
                    $regrasDinamicas[$input] = $regra;
                }
            }

            $request->validate($regrasDinamicas);

        } else {
            $request->validate($school_area->rules());
        }
        if($request->file('photo')) {
            Storage::disk('public')->delete($school_area->photo);
        }

        $photo = $request->file('photo');
        $photo_urn = $photo->store('/', 'public');

        $school_area->fill($request->all());
        $school_area->photo = $photo_urn;
        $school_area->save();

        return response()->json($school_area, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SchoolArea  $school_area
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $school_area = $this->school_area->find($id);

        if($school_area === null) {
            return response()->json(['erro' => 'Impossível realizar a exclusão. O recurso solicitado não existe'], 404);
        }
        Storage::disk('public')->delete($school_area->photo);

        $school_area->delete();
        return response()->json(['msg' => 'O modelo foi removido com sucesso!'], 200);

    }
}
