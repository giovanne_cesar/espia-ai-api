<?php


namespace App\Services;


use App\Models\User;

interface UserServiceInterface extends BaseServiceInterface
{
    public function getUserByEmail(string $email): User;
}
