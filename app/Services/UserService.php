<?php
namespace App\Services;

use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\Traits\PaginateService;
use Illuminate\Support\Facades\Hash;


class UserService extends BaseService implements UserServiceInterface
{

    public function __construct(UserRepository $repository)
    {
        parent::__construct($repository);
        $this->repository = $repository;
    }

    public function store($data)
    {
        $data['password'] = Hash::make($data['password']);
        return $this->repository->create($data);
    }

    public function getUserByEmail(string $email): User
    {
        return $this->repository->findWhere(compact('email'))->first();
    }
}
