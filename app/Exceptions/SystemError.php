<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;

class SystemError extends Exception
{
    public function errorMessage() {
        $errorMsg = $this->getMessage();
        return $errorMsg;
    }
}