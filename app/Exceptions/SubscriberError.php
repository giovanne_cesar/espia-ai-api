<?php

namespace App\Exceptions;

use Exception;

class SubscriberError extends Exception
{
    public function errorMessage(): string
    {
        return $this->getMessage();
    }
}
